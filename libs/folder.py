"""Smart folder with indexing.

This module reads a folder structure and creates a new session folder on demand.

"""
import os
import shutil


class Folder:
    """Smart Folder class.

    Args:
        path (str): A path to root folder.
        enumerate_folders (bool): Enumeration usage indicator.
        name_format (str): A string name_format used for session enumeration.

    Raises:
        OSError: An error occurred while creating a folder.
    """
    def __init__(self, path, enumerate_folders=True, name_format="%04d",
                 recreate=False):
        self.root = os.path.abspath(path)
        self.enumerate_folders = enumerate_folders
        self.name_format = name_format

        Folder.makedirs_helper(self.root, force=recreate)

        if not self.enumerate_folders:
            self.session = self.root
        else:
            index = 0
            while True:
                if not os.path.isdir(
                        os.path.join(self.root, self.name_format % index)
                    ):
                    break
                index += 1

            self.session = os.path.join(self.root, self.name_format % index)
            Folder.makedirs_helper(self.session)

            print('[session]', self.session)

    def makedirs(self, path, force=True):
        """
        Folder.makedirs_helper() wrapper for relative paths.

        Args:
            path (str): A path of folders to create.
            force (bool): Overwrite indicator.

        Returns:
            If successful return path, else return None.
        """
        path = os.path.join(self.session, path)
        if not Folder.makedirs_helper(path, force):
            return None
        return path


    @staticmethod
    def makedirs_helper(path, force=True):
        """
        Simple os.makedirs() wrapper.

        Args:
            path (str): A path of folders to create.
            force (bool): Overwrite indicator.

        Returns:
            bool: True if successful, False otherwise.
        """

        try:
            os.makedirs(path)
        except FileExistsError:
            if not force:
                return False

            shutil.rmtree(path)
            os.makedirs(path)

        return True

    @classmethod
    def training(cls):
        """
        Create a default training folder.

        Returns:
            Folder: A ./training Folder instance

        """
        return cls("./training")

    @classmethod
    def validation(cls):
        """
        Create a default validation folder.

        Returns:
            Folder: A ./validation Folder instance

        """
        return cls("./validation")

    @classmethod
    def prediction(cls):
        """
        Create a default prediction folder.

        Returns:
            Folder: A ./prediction Folder instance
        """
        return cls("./prediction")
