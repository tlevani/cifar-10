"""
Neural network layers implementations.
"""

import tensorflow as tf
import tensorflow.contrib.slim as slim


def conv2d_block(network, num_outputs, repeat=2, scope="conv2d_block",
                 activation=None, stride=2, filters=(5, 5)):
    """
    Create a convolutional block. Use striding instead of pooling.

    Args:
        network (Model): A network Model instance to use.
        num_outputs (int): Number of filters per convolutional layer.
        repeat (int): Number of convolutional layers in convolutional block.
        scope (str): Variable scope name.

    Returns:
        Model: Updated network Model.
    """
    assert repeat > 0, "Number of convolutional layers must be at least 1"

    with tf.variable_scope(scope):
        initializer = tf.variance_scaling_initializer()
        regularizer = slim.l2_regularizer(1e-3)
        padding = 'same'

        layer = network.output
        for i in range(repeat - 1):
            layer = slim.conv2d(
                layer, num_outputs, filters, 1,
                padding=padding,
                weights_initializer=initializer,
                weights_regularizer=regularizer,
                activation_fn=activation,
                scope='conv2d_{}'.format(i)
            )

        layer = slim.conv2d(
            layer, num_outputs, filters, stride,
            padding=padding,
            weights_initializer=initializer,
            weights_regularizer=regularizer,
            activation_fn=activation,
            scope='conv2d_{}'.format(repeat)
        )

    network.endpoints[scope] = layer

    network.outputs.append(layer)
    return network


def dropout(network, keep_prob=0.5, is_training=False, scope="dropout"):
    """
    Create a dropout layer.

    Args:
        network (Model): A network Model instance to use.
        keep_prob (float): The dropout keep probability, between 0 and 1. E.g.
            "keep_prob=0.9" would drop out 10% of input units.
        is_traini Python boolean, or a TensorFlow boolean scalar
            tensor (e.g. a placeholder). Whether to return the output in
            training mode (apply dropout) or in inference mode (return the
            input untouched).
        scope (str): Variable scope name.

    Returns:
        Model: Updated network Model.
    """

    with tf.variable_scope(scope):
        layer = slim.dropout(
            network.output,
            keep_prob=keep_prob,
            is_training=is_training
        )

    network.outputs.append(layer)
    return network


def flatten(network, scope='flatten'):
    """
    Flatten the last network layer.

    Args:
        network (Model): A network Model instance to use.
        scope (str): Variable scope name.

    Returns:
        Model: Updated network Model.
    """

    with tf.variable_scope(scope):
        layer = tf.layers.flatten(network.output)

    network.endpoints[scope] = layer
    network.outputs.append(layer)
    return network


def fully_connected(network, num_outputs, scope="fc",
                    activation=None, initializer=None):
    """
    Add a fully connected layer to network model.

    Args:
        network (Model): A network Model instance to use.
        num_outputs (int): The number of output units in the layer.
        scope (str): Variable scope name.
        activation: Activation function.
        initializer: Custom weight initializer. 

    Returns:
        Model: Updated network Model.
    """

    with tf.variable_scope(scope):
        if initializer is None:
            initializer = tf.variance_scaling_initializer()

        regularizer = slim.l2_regularizer(1e-3)

        fully = slim.fully_connected(
            network.output,
            num_outputs,
            activation_fn=activation,
            weights_initializer=initializer,
            weights_regularizer=regularizer
        )

    network.endpoints[scope] = fully
    network.outputs.append(fully)
    return network
