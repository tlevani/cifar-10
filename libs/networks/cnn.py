"""
CNN network model.
"""
import datetime
import json
import os
import time

import numpy as np
import tensorflow as tf

from . import layers, model

FLAGS = tf.app.flags.FLAGS


class CNN(model.Model):
    """
    Convolutional network.

    Args:
        width (int): Input image width.
        height (int): Input image height.
        categories (int): Number of categories.
        data_augmentation (bool): Data augmentation usage indicator.
    """
    def __init__(self, width=32, height=32, categories=10,
                 data_augmentation=False):
        self.width = width
        self.height = height
        self.categories = categories

        self.summary_writer = None
        self.validation_summary_writer = None
        self.summary = None
        self.validation_summary = None

        self.data_augmentation = data_augmentation
        self._input_layer_keep_rate = 1.0 if data_augmentation else 0.8

        features = tf.placeholder(tf.float32, shape=[None, width, height, 3])
        super().__init__(features, "CNN")

        with tf.variable_scope(self.name):
            self.is_training = tf.placeholder_with_default(False, shape=())
            is_training = self.is_training

            self = layers.dropout(
                self, self._input_layer_keep_rate, is_training=is_training
            )

            self = layers.conv2d_block(
                self, 96, 3, 'conv1', tf.nn.leaky_relu, 2, [3, 3]
            )
            self = layers.dropout(self, 0.9, is_training=is_training)

            self = layers.conv2d_block(
                self, 192, 3, 'conv3', tf.nn.leaky_relu, 2, [3, 3]
            )
            self = layers.dropout(self, 0.9, is_training=is_training)

            self = layers.flatten(self)

            self = layers.fully_connected(
                self, 1024, 'fc1', tf.nn.leaky_relu,
                initializer=tf.variance_scaling_initializer()
            )
            self = layers.dropout(self, 0.5, is_training=is_training)

            self = layers.fully_connected(
                self, 1024, 'fc2', tf.nn.leaky_relu,
                initializer=tf.variance_scaling_initializer()
            )
            self = layers.dropout(self, 0.5, is_training=is_training)

            self = layers.fully_connected(
                self, categories, 'fc3', None,
                initializer=tf.contrib.layers.xavier_initializer()
            )

    def predict(self, dataset, checkpoint):
        """
        Run CNN model on a given dataset.

        Args:
            dataset (Dataset): A Dataset instance used as input.
            checkpoint (str): A path to generator network weights checkpoint.
        """
        with tf.Session() as sess:
            tf.train.Saver().restore(sess, checkpoint)
            self.session = sess
            predictions = np.zeros(dataset.size)
            real = np.zeros(dataset.size)

            counter = 0
            for images, labels in dataset(FLAGS.batch_size):
                predictions[counter:counter + len(labels)] = tf.argmax(
                    tf.nn.softmax(self(images)), axis=1
                ).eval()
                real[counter:counter + len(labels)] = labels
                counter += len(labels)
                print('{} examples finished'.format(counter), end='\r')
            print()

            x = tf.confusion_matrix(real, predictions).eval(session=sess)
            accuracy = tf.reduce_sum(tf.diag_part(x)).eval(session=sess) / \
                       dataset.size
            print(x)

            print("Accuracy =", accuracy)

    def loss(self):
        """
        Create loss function and summary elements.

        Returns:
            tuple: A tuple with tensorflow graph input nodes

        Todo:
            * Use dict instead of tuple.
            * Add streaming summary variables. (Accuracy, precision, ...)
        """

        global_step = tf.Variable(
            FLAGS.global_step, name='global_step', trainable=False
        )

        labels = tf.placeholder(tf.int32, shape=[None])

        softmax_loss = tf.reduce_mean(
            tf.nn.sparse_softmax_cross_entropy_with_logits(
                labels=labels,
                logits=self.output
            )
        )
        tf.summary.scalar("softmax_loss", softmax_loss,
                          collections=[tf.GraphKeys.SUMMARIES, 'validation'])

        tf.summary.histogram(
            "variables/prediction_error",
            tf.abs(tf.one_hot(labels, self.categories) - self.output),
            collections=[tf.GraphKeys.SUMMARIES, 'validation']
        )

        regularization_losses = tf.get_collection(
            tf.GraphKeys.REGULARIZATION_LOSSES
        )
        loss = tf.add_n(
            [softmax_loss] + regularization_losses
        )

        tf.summary.scalar("loss", loss,
                          collections=[tf.GraphKeys.SUMMARIES, 'validation'])

        predictions = tf.cast(
            tf.argmax(tf.nn.softmax(self.output), axis=1), dtype=tf.int32
        )

        accuracy = tf.contrib.metrics.accuracy(labels, predictions, name='acc')
        tf.summary.scalar("accuracy", accuracy,
                          collections=[tf.GraphKeys.SUMMARIES, 'validation'])

        confusion_matrix = tf.confusion_matrix(labels, predictions, 10)

        diag = tf.diag_part(confusion_matrix)
        row_sum = tf.reduce_sum(confusion_matrix, axis=1)
        col_sum = tf.reduce_sum(confusion_matrix, axis=0)

        precision = diag / row_sum
        recall = diag / col_sum

        for i in range(10):
            tf.summary.scalar(
                "precision/{}".format(i), precision[i],
                collections=[tf.GraphKeys.SUMMARIES, 'validation']
            )

        for i in range(10):
            tf.summary.scalar(
                "recall/{}".format(i), recall[i],
                collections=[tf.GraphKeys.SUMMARIES, 'validation']
            )

        f1_score = 2 * (precision * recall)
        s = precision + recall
        f1_score = tf.where(
            tf.logical_or(tf.is_nan(f1_score), tf.less(s, 1e-7)),
            tf.zeros(10, dtype=tf.float64),
            f1_score / s
        )
        for i in range(10):
            tf.summary.scalar(
                "f1_score/{}".format(i), f1_score[i],
                collections=[tf.GraphKeys.SUMMARIES, 'validation']
            )

        lr_config = CNN.read_json(FLAGS.learning_rate_config)

        learning_rate = tf.train.piecewise_constant(
            global_step,
            lr_config['iteration_boundaries'],
            lr_config['learning_rate_values'],
            name='piecewise_constant_learning_rate'
        )
        tf.summary.scalar('learning_rate', learning_rate)

        adam = tf.train.AdamOptimizer(
            learning_rate=learning_rate,
            beta1=FLAGS.adam_beta1,
            name='adam'
        )

        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            minimize = adam.minimize(
                loss,
                var_list=self.variables,
                name='loss_minimize',
                global_step=global_step
            )

        return self.input, labels, loss, minimize

    def train(self, dataset, folder, epochs=1000):
        """
        Train CNN model on a given dataset. Validate loss function values
        using a validation Dataset.

        Args:
            dataset (Dataset): A Dataset instance used for training.
            folder (Folder): A Folder used saving results.
            epochs (int): Number of epochs to run.
        """
        checkpoint = folder.makedirs('checkpoints')
        os.makedirs(FLAGS.logdir, exist_ok=True)

        images, labels, loss, minimize = self.loss()

        saver = tf.train.Saver(self.variables)
        sess = tf.Session()

        init_op = tf.group(
            tf.global_variables_initializer(),
            tf.local_variables_initializer()
        )
        sess.run(init_op)
        self.session = sess

        if FLAGS.checkpoint is not None:
            s = tf.train.Saver([v for v in self.variables if 'CNN' in v.name])
            s.restore(sess, FLAGS.checkpoint)

        grads = tf.gradients(loss, tf.trainable_variables());
        for v, g in zip(tf.trainable_variables(), grads):
            tf.summary.histogram(v.name, v)
            tf.summary.histogram(v.name + '/gradient', g)

        step = FLAGS.global_step
        example = 0
        start_time = time.time()

        variable = self.variables[0]
        diff = tf.placeholder(dtype=tf.float32, shape=variable.shape)
        tf.summary.histogram('weight_difference', diff)

        self.summary_writer = tf.summary.FileWriter(
            os.path.join(FLAGS.logdir, FLAGS.name),
            graph=tf.get_default_graph()
        )
        self.validation_summary_writer = tf.summary.FileWriter(
            os.path.join(FLAGS.logdir, FLAGS.name + '_validation'),
            graph=tf.get_default_graph()
        )

        self.summary = tf.summary.merge_all()
        self.validation_summary = tf.summary.merge_all(key='validation')

        print('epoch, step, examples, loss, time')
        for epoch in range(epochs):
            for images_batch, labels_batch in dataset(FLAGS.batch_size):
                batch_size = len(images_batch)
                example += batch_size

                feed_dict = {
                    images: images_batch,
                    labels: labels_batch,
                    self.is_training: True
                }

                if step % FLAGS.checkpoint_period == 0:
                    saver.save(
                        sess,
                        os.path.join(checkpoint, 'model.ckpt'),
                        global_step=step
                    )

                    # TODO: use smaller batches and average the summary values
                    for im, la in dataset.validation(dataset.validation_size):
                        fd = {
                            images: im,
                            labels: la,
                            self.is_training: False,
                        }

                        _, summary = sess.run(
                            [loss, self.validation_summary],
                            feed_dict=fd
                        )

                    self.validation_summary_writer.add_summary(summary, step)

                if step % FLAGS.verbose_step != 0:
                    _, _loss = sess.run(
                        [minimize, loss],
                        feed_dict=feed_dict
                    )
                else:
                    _old = variable.eval(session=sess)
                    _, _loss, = sess.run(
                        [minimize, loss],
                        feed_dict=feed_dict
                    )

                    feed_dict[diff] = variable.eval(session=sess) - _old

                    _, summary = sess.run(
                        [diff, self.summary],
                        feed_dict=feed_dict
                    )

                    self.summary_writer.add_summary(summary, step)

                    print("{}, {}, {}, \033[32m{:f}\033[0m, {}".format(
                        epoch,
                        step,
                        example,
                        _loss,
                        datetime.timedelta(seconds=time.time() - start_time)
                    ), end='\r')

                step += 1

        saver.save(
            sess,
            os.path.join(checkpoint, 'model.ckpt'),
            global_step=step
        )

        print()
        print('Finished')

    @staticmethod
    def read_json(path):
        """JSON file parsing wrapper.

        Args:
            path (str): File path.

        Returns:
            dict: A dictionary from JSON file.
        """
        with open(path, 'r') as f:
            return json.load(f)
