"""
A CIFAR-10 based dataset module with a batch generator.
"""

import glob
import pickle

import numpy as np
from imgaug import augmenters as iaa


class Dataset:
    """
    A class representing dataset.

    Args:
        path (str): Folder containing CIFAR-10 dataset.
        data_augmentation (bool): Data augmentation usage indicator.
        validation_percentage (float): Percentage of data to use as validation.
    """
    WIDTH = 32
    HEIGHT = 32

    def __init__(self, path, data_augmentation=False,
                 validation_percentage=0.0):
        self.path = path
        self.data_augmentation = data_augmentation
        self.validation_percentage = validation_percentage

        self.seq = iaa.Sequential([
            iaa.Fliplr(0.5),
            iaa.Crop(percent=(0, 0.1)),
            iaa.Multiply((0.8, 1.2), per_channel=0.2),
            iaa.Affine(
                scale={"x": (0.8, 1.2), "y": (0.8, 1.2)},
                translate_percent={"x": (-0.05, 0.05), "y": (-0.05, 0.05)},
                rotate=(-15, 15)
            )
        ], random_order=True)

        self._data = [self._read_file(f) for f in glob.glob(path)]

        assert self._data, "Number of input files must be at least 1"

        _images = [x['images'] for x in self._data]
        _labels = [x['labels'] for x in self._data]

        self._data = {
            'images': np.concatenate(_images),
            'labels': np.concatenate(_labels)
        }

        size = len(self._data['labels'])
        p = np.random.permutation(size)

        train_data_size = size - int(size * self.validation_percentage)
        self._train_data = {
            'images': self._data['images'][p][:train_data_size],
            'labels': self._data['labels'][p][:train_data_size]
        }

        self._validation_data = {
            'images': Dataset.per_image_standardization(
                self._data['images'][p][train_data_size:]
            ),
            'labels': self._data['labels'][p][train_data_size:]
        }

    @staticmethod
    def per_image_standardization(batch):
        """Apply per image standardization to a batch of images.

        For each element x in an image set value equal to

                            (x - mean) / adjusted_stddev,

        where mean is the average of all values in an image, and

                adjusted_stddev = max(stddev, 1.0 / sqrt(image.size)),

        stddev is the standard deviation of all values in image. It is capped
        away from zero to protect against division by 0 when handling uniform
        images.

        Args:
            batch (numpy.ndarray): An array of RGB images.

        Returns:
            numpy.ndarray: Input array with applied standardization.
        """

        images = batch.astype(np.float)
        for image in images:
            mean = np.mean(image)
            image -= mean

            stddev = np.std(image)
            adjusted_stddev = max(stddev, 1.0 / np.sqrt(image.size))

            if adjusted_stddev != 0:
                image /= adjusted_stddev

        return images

    def _read_file(self, filename):
        dictionary = Dataset.unpickle(filename)
        pix = np.array(dictionary[b'data'], dtype=np.uint8)

        # Reshape the array to 4-dimensions.
        images = pix.reshape([-1, 3, Dataset.WIDTH, Dataset.HEIGHT])
        # Reorder the indices of the array.
        images = images.transpose([0, 2, 3, 1])

        if not self.data_augmentation:
            images = Dataset.per_image_standardization(images)

        return {
            'images': images,
            'labels': np.array(dictionary[b'labels'], dtype=np.int32)
        }

    def _get_batches(self, images, labels, batch_size=256, validation=False):
        """Helper function for generating dataset batches.

        Args:
            images (numpy.ndarray): Numpy array of images.
            labels (numpy.ndarray): Numpy array of labels.
            batch_size (int, optional): Defaults to 256. Maximum batch size.
            validation (bool, optional): Defaults to False. Validation set
                indicator.

        Yields:
            (numpy.ndarray, numpy.ndarray): An ordered pair of images and
                labels containing at most batch_size elements.
        """
        size = len(labels)
        p = np.random.permutation(size)

        if not validation and self.data_augmentation:
            images = Dataset.per_image_standardization(
                self.seq.augment_images(images)
            )

        for batch in range(self.batches(batch_size, size)):
            yield images[p][batch * batch_size:(batch + 1) * batch_size], \
                  labels[p][batch * batch_size:(batch + 1) * batch_size]

    def validation(self, batch_size=256):
        """Validation dataset batch generator

        Generate multiple batches of images from this Dataset instance
        validation set.

        Args:
            batch_size (int): A number of examples per batch.

        Yields:
            (numpy.ndarray, numpy.ndarray): An ordered pair of images and
                labels containing at most batch_size elements.
        """
        yield from self._get_batches(
            self._validation_data['images'],
            self._validation_data['labels'],
            batch_size,
            True
        )

    def __call__(self, batch_size=256):
        """Dataset batch generator

        Generate multiple batches of images from this Dataset instance.

        Args:
            batch_size (int): A number of examples per batch.

        Yields:
            (numpy.ndarray, numpy.ndarray): An ordered pair of images and
                labels containing at most batch_size elements.
        """
        yield from self._get_batches(
            self._train_data['images'],
            self._train_data['labels'],
            batch_size
        )

    @property
    def size(self):
        """Dataset length.

        Number of images in a dataset instance.

        Returns:
            int: Dataset length.
        """
        return len(self._train_data['labels'])

    @property
    def validation_size(self):
        """Validation dataset length.

        Number of images in validation dataset.

        Returns:
            int: Validation dataset length.
        """
        return len(self._validation_data['labels'])

    def batches(self, batch_size, size=None):
        """Number of batches.

        Number of batches for a given batch_size.

        Args:
            batch_size (int): Batch size to use.

        Returns:
            int: Number of batches.
        """
        if size is None:
            size = self.size

        return size // batch_size + (size % batch_size > 0)

    @staticmethod
    def unpickle(path):
        """CIFAR-10 compatible unpickle helper.

        Args:
            path (str): File path.

        Returns:
            dict: A dictionary based on CIFAR-10 pickle rules.
        """
        with open(path, 'rb') as f:
            dictionary = pickle.load(f, encoding='bytes')
        return dictionary
