"""
Simple program used for model training.
"""

import tensorflow as tf

from libs import Dataset, Folder
from libs.networks import CNN


FLAGS = tf.app.flags.FLAGS


tf.app.flags.DEFINE_float(
    'adam_beta1',
    0.9,
    "Adam optimizer beta1 argument"
)

tf.app.flags.DEFINE_float(
    'adam_beta2',
    0.999,
    "Adam optimizer beta2 argument"
)

tf.app.flags.DEFINE_float(
    'adam_epsilon',
    0.1,
    "Adam optimizer epsilon argument"
)

tf.app.flags.DEFINE_integer(
    'batch_size',
    256,
    "Number of samples per batch"
)

tf.app.flags.DEFINE_integer(
    'checkpoint_period',
    100,
    "Number of iterations (batch steps) in between checkpoints"
)

tf.app.flags.DEFINE_string(
    'dataset',
    './dataset',
    "Path to the dataset directory"
)

tf.app.flags.DEFINE_string(
    'logdir',
    './logs',
    "Path to the tensorboard logs directory"
)

tf.app.flags.DEFINE_string(
    'name',
    'unnamed',
    "Tensorboard log name"
)

tf.app.flags.DEFINE_integer(
    'epochs',
    500,
    "Number of epochs to train the model"
)

tf.app.flags.DEFINE_string(
    'learning_rate_config',
    './learning_rate_config.json',
    'A file containing learning rate piecewise constant rules in JSON format'
)

tf.app.flags.DEFINE_string(
    'checkpoint',
    None,
    'If given, continue training from this checkpoint'
)

tf.app.flags.DEFINE_integer(
    'global_step',
    0,
    'Initial value for global step'
)

tf.app.flags.DEFINE_integer(
    'verbose_step',
    10,
    'Print loss to terminal each `verbose_step` step'
)


def main(argv=None):
    """
    Initialize CNN and CIFAR-10 dataset and run training on it. This function
    is called from its tf.app.run() wrapper.

    Args:
        argv (list, optional): Defaults to None. Dummy argument used by
            tf.app.run().
    """
    dataset = Dataset(
        FLAGS.dataset,
        True,
        0.02
    )

    cnn = CNN(32, 32, 10)
    cnn.train(
        dataset,
        Folder.training(),
        epochs=FLAGS.epochs
    )


if __name__ == '__main__':
    tf.app.run()
