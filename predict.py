"""
Simple program used for prediction.
"""

import os

import tensorflow as tf

from libs import Dataset
from libs.networks import CNN


FLAGS = tf.app.flags.FLAGS


tf.app.flags.DEFINE_integer(
    'batch_size',
    256,
    "Number of samples per batch."
)

tf.app.flags.DEFINE_string(
    'checkpoint',
    './checkpoints/model.ckpt',
    "Checkpoint file location"
)

tf.app.flags.DEFINE_string(
    'dataset',
    './dataset',
    "Path to the dataset directory."
)


def main(argv=None):
    """
    Initialize CNN and CIFAR-10 compatible dataset and run predictions on it.
    This function is called from its tf.app.run() wrapper.

    Args:
        argv (list, optional): Defaults to None. Dummy argument used by
            tf.app.run().
    """

    dataset = Dataset(FLAGS.dataset)
    cnn = CNN(32, 32, 10)
    cnn.predict(dataset, FLAGS.checkpoint)


if __name__ == '__main__':
    tf.app.run()
