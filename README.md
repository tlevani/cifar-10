# CIFAR-10 classification

Source code accompanying my master's thesis at Faculty of Science, University
of Zagreb. The code provides simple CNN implementation for training and
predicting on `CIFAR-10` dataset.

## dependencies

`python3` and all Python dependencies listed in `requirements.txt`.

***

## train

```bash
python train.py --batch_size 256 \
                --dataset "path/to/data_batch_*" \
                --epochs 250 \
                --name "tensorboard_log_name"
```

For more info run `python train.py --help`.

***

## predict

```bash
python predict.py --batch_size 256 \
                  --checkpoint "path/to/checkpoint/model.ckpt" \
                  --dataset "path/to/test_batch"
```

For more info run `python predict.py --help`.

***
